# Напишите приммеры использования всех операций со словарями
    # copy()
car = {
    'name': 'BMW',
    'nom': '1997',
    'year': 2000
}
car_1 = car.copy()
print(car_1)


    # fromkeys()
car_1 = car.fromkeys('name', 'cho hochesh')
print(car_1)

    # get()
car.get('name')
print(car.get('nom'))

    # items
mashina = car.items()
print(mashina)


# keys()
k = car.keys()
print(k)

# values()
beha = car.values()
print(beha)

# pop()
popped = car.pop('nom')
print(popped)
# print(car)


# popitem()
n = car.popitem()
print(n)
# clear()
car = {
    'name': 'BMW',
    'nom': '1997',
    'year': 2000
}
car.clear()
print(car)


# Оберните все операции в функции, которые принимают словарь и выполняют над ним операцию. Функцию надо вызвать.
    # copy()
dict = {
    'name': 'kadyrbek',
    'age': 23,
}
def kad(dict):
    print(dict.copy())

kad(dict)

    # fromkeys()
dic = {'a', 'b', 'c', 'd', 'e'}
dic1 = 'Value'
def kad1(dic):
    print(dict.fromkeys(dic, dic1))

kad1(dic)

    # get()
doc = {
    'name': 'aigul',
    'age': 19
}
def kad2(doc):
    n = (doc.get('name'))
    print(n)
kad2(doc)

    # items()
def kad3(doc):
    print(doc.items())

kad3(doc)
    # keys()
def kad4(doc):
    print(doc.keys())
kad4(doc)
    # values()
def kad5(doc):
    print(doc.values())

kad5(doc)
    # pop()
def kad6(doc):
    print(doc.pop('name'))

kad6(doc)
    # popitem()
def kad7(doc):
    print(doc.popitem())

kad7(doc)
    # clear()
def kad8():
    doc.clear()
    print(doc)

kad8()
# Задача для гугления и самостоятельной рабооты. Разобрраться как работает метод dict.update() и dict.setdefault()
# dict.update()
dicc = {
    'name': 'Umar',
    'age': 17
}
dicc2 = {
    'age': 18
}
dicc.update(dicc2)
print(dicc)

# dict.setdefault()
dicc.setdefault('name')
print(dicc)
dicc2.setdefault('age')
print(dicc2)
dicc.setdefault('year')
print(dicc)

# Напишите пример вложенной функции.


def kady(number1, number2):
    def mady():
        print(number1 + number2)
    return mady()

kady(145, 190)


# Напишите функцию принимающую массив (состоящий из слов, название файла) и при помощи данных аргументов создающую запись в файле
massiv = ['My', ' name ', 'is ', 'Kadyrbek']
def defu(sozdor):
    file = open('umar.txt', 'w')
    for soz in sozdor:
        file.write(soz)
    file.close()

defu(massiv)

# Напишите функцию принимающую массив (состоящий из слов, название файла) и при помощи данных аргументов дополняющую файл новыми записями
massiv1 = ['I', ' am ', 19, 'years', 'old']
def deu(sozdor1):
    file = open('umarbek.txt', 'a')
    for soz1 in sozdor1:
        file.write(str(soz1) + '\n')
    file.close()

deu(massiv1)


# Напишите функцию считывающую данные из файла
def okuit_dannyilardy():
    file = open('Umar.txt', 'r')
    print(file.readlines())
    file.close()

okuit_dannyilardy()

# Напишите функцию записи в файл которая приниммает в себя данные, отфильтровывает их и записывает только отфильтрованные данные
kursy = ['ryzdovoi1', 123, 'ryadovoi2', 122, 'ryadovoi3', 121, 'ryadovoi4', 120]

def filter(sandar):
    file = open('sandar.txt', 'a')
    for san in sandar:
        if type(san) != str:
            file.write(str(san) + '\n')
    file.close()

filter(kursy)
